//exercise 1----------------------------------------------------
//without running the code, state the values of all variables ()
let x = 10, y = 30;
let a = ++x; 
let b = y++; 
let c = ++x+yy++;

//exercise 2----------------------------------------------------
//what is the ouput?
3+2+'4'
'4'+2+2
'4'+2*2
'4'+(3+2)

"DB  " + 10
"DB  " - 10
"  " - 10
"  " + 10

//exercise 3----------------------------------------------------
//explain why such ouput?
(3>2) + (3<2) ==> 1
(3>2) + 3<2 ==> false
3>2 + 3<2 ==> true

6 + "3" ==> "63"
6 / "3" ==> 2, 6 * "3" ==> 18, 6 - "3" ==> 3

//exercise 4----------------------------------------------------
//what is the ouput?
'6%'+3 , '6'%3
'6%'-3, '6%'*3, '6%'/3 

//exercise 5----------------------------------------------------
//what is the ouput?
undefined+1 ==>
null+1 ==>

null+null 
null-null
null/null
5/null
5/0

null==null
null==0 //== does NOT coerece null to 0
why +null==0  //works fine?
is null 0?
null>=0  //== does coerece null to 0

undefined==null //(special case)
undefined === null 


typeof undefined + 0
typeof (undefined + 0)
//undefined is always converted (coerced) to NaN in a number expression
//null is always converted (coerced) to 0 in a number expression (except ==)


//exercise 6----------------------------------------------------
//what is the ouput and why such output?
3>2+5<6
typeof 3>2+5<6
typeof (3>2)+5<6
typeof (3>2+5)<6
typeof (3>2+5<6)

//--------------------------------------------------------------
//exercise 7-
Write two prompt statments to ask user to enter two numbers. 
Then ouput the sum of the two numbers.
